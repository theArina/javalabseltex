import java.util.*;

public class ShoppingCart<T extends Electronics> {

    List<T> goods = new LinkedList<>();
    Set<UUID> ids = new HashSet<>();

    public int ifContinue() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to continue?[0 - no/1 - yes]");
        char check = sc.next().charAt(0);

        if (check == '1') {
            pickGood();
        } else if (check == '0') {

            return 0;
        } else {
            System.out.println("What? Try again\n");
            ifContinue();
        }
        return 0;
    }

    public int pickGood() {

        Scanner sc = new Scanner(System.in);
        System.out.println("What do you want to pick?\n1. Phone\n2. Smartphone\n3. Tablet\n");
        String goodName = sc.nextLine();
        goodName = goodName.toLowerCase();
        T good;

        if (goodName.equals("phone") || goodName.equals("1")) {
            good = (T)new Phones();
        } else if (goodName.equals("smartphone") || goodName.equals("2")) {
            good = (T)new Smartphones();
        } else if (goodName.equals("tablet") || goodName.equals("3")) {
            good = (T)new Tablets();
        } else {
            System.out.println("Wrong name of the object. Try again\n");
            pickGood();
            return 0;
        }
        good.update();
        addGood(good);
        System.out.println("\nYour good added in the cart!");

        ifContinue();

        return 0;
    }

    public void addGood(T good) {
        goods.add(good);
        ids.add(good.id);
    }

    public void deleteGood(Electronics el) {
        goods.remove(el);
    }

    public void showAllObjects() {
        for (Electronics n : goods) {
            n.read();
        }
    }

    public boolean searchById(UUID id) {

        return ids.contains(id);
    }
}
