import java.util.Scanner;
import java.util.UUID;

public class Smartphones extends Electronics {

    private int simType;
    private int simsAmount;
    private String SIMTYPES[] = {"micro", "ordinary"};

    Smartphones() {
        super();
    }

    Smartphones(UUID id) {
        super(id);
    }

    @Override
    public void create() {
        super.create();
        simType = (int)(Math.random() * SIMTYPES.length);
        simsAmount = 1;
    }

    @Override
    public void read() {
        System.out.println("\n--Smartphone--");
        super.read();
        System.out.println("Sim type: " + SIMTYPES[simType]);
        System.out.println("Amount of sims: " + simsAmount);
    }

    @Override
    public void update() {
        super.update();
        Scanner sc = new Scanner(System.in);
        System.out.println("Sim type [0 - micro; 1 - ordinary]:");
        this.simType = sc.nextInt();

        System.out.println("Amount of sims [1-4]:");
        this.simsAmount = sc.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        simType = 0;
        simsAmount = 0;
    }
}