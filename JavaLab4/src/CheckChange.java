public class CheckChange extends ACheck {

    public void run() {
        while (true) {
            for (Order n : App.orders.orders) {
                if (!App.orders.checkOrder(n)) {
                    n.setOrderStatus(true);
                    System.out.println("Turned " + App.orders.orders.indexOf(n) + " order into \"processed\"");
                }
            }
            try {
                Thread.sleep(1300);
                System.out.println("- I'm thread 2, i've slept for 1300 millis");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
