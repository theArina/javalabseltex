import java.util.Scanner;
import java.util.UUID;

public class Phones extends Electronics {

    private int phoneCase;
    private String PHONECASES[] = {"classic", "clamshell"};

    Phones() {
        super();
    }

    Phones(UUID id) {
        super(id);
    }

    @Override
    public void create() {
        super.create();
        phoneCase = (int)(Math.random() * PHONECASES.length);
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Case: " + PHONECASES[phoneCase]);
    }

    @Override
    public void update() {
        super.update();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter type of case [0 - classic; 1 - clamshell]:");
        this.phoneCase = sc.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        phoneCase = 0;
    }
}
