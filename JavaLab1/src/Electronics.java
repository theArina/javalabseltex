import java.util.Scanner;
import java.util.UUID;

public abstract class Electronics implements ICrudAction {

    static private int count;
    UUID id;

    private String name;
    private int price;
    private String firm;
    private String model;
    private String os;

    Electronics() {
        id = UUID.randomUUID();
    }

    Electronics(UUID id) {
        this.id = id;
    }

    @Override
    public void create() {
        this.price = 10000;
        this.name = "SomeName";
        this.firm = "Sony";
        this.model = "12345";
        this.os = "Android";
        count++;
    }

    @Override
    public void read() {
        System.out.println("Price: " + price);
        System.out.println("Name: " + name);
        System.out.println("Firm: " + firm);
        System.out.println("Model: " + model);
        System.out.println("OS: " + os);
    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter wishful price:");
        this.price = sc.nextInt();
        sc.nextLine();

        System.out.println("Enter name:");
        this.name = sc.nextLine();

        System.out.println("Enter firm:");
        this.firm = sc.nextLine();

        System.out.println("Enter model:");
        this.model = sc.nextLine();

        System.out.println("Enter OS:");
        this.os = sc.nextLine();
//        sc.close();
    }

    @Override
    public void delete() {
        this.price = 0;
        this.name = "0";
        this.firm = "0";
        this.model = "0";
        this.os = "0";
        count--;
    }
}
