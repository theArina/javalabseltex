import java.util.Scanner;

public class App {
    public static void main(String args[]) {

        int count = Integer.valueOf(args[0]);
        Electronics arr[] = new Electronics[count];

        args[1] = args[1].toLowerCase();

        System.out.println("Do you want to create objects by default or pick them by yourself?[0/1]");
        Scanner sc = new Scanner(System.in);
        int check = sc.nextInt();

        for (int i = 0; i < count; i++) {
            switch (args[1]) {
                case "phone":
                    arr[i] = new Phones();
                    break;
                case "smartphone":
                    arr[i] = new Smartphones();
                    break;
                case "tablet":
                    arr[i] = new Tablets();
                    break;
                default:
                    i = -1;
                    System.out.println("Wrong name of the object");
                    break;
            }

            if (i != -1 && check == 1) {
                arr[i].update();
            } else if (i != -1 && check == 0) {
                arr[i].create();
                System.out.println("Creating " + (i + 1) + " object...");
            } else {
                break;
            }
        }

        System.out.println("Do you want to see created objects?[0 - yes/1 - no]");
        int show = sc.nextInt();
        sc.close();

        if (show == 0) {
            for (int i = 0; i < count; i++) {
                System.out.println("\nObject " + (i + 1));
                arr[i].read();
            }
        }
//        arr[i].delete();
    }
}