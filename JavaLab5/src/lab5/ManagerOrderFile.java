package lab5;
import main.*;

import java.io.*;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {

    @Override
    public void readAll(String file) {

        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(fis);
            Order ts = (Order) oin.readObject();

            while (ts != null) {
                ts.showOrder();
                ts = (Order) oin.readObject();
            }
            oin.close();

        } catch (Exception e) {
            System.out.println("File not found or idk");
        }
    }

    @Override
    public void readById(String file, UUID id) {

        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(fis);
            Order ts = (Order) oin.readObject();
            System.out.println(fis.available());

            while (ts != null) {
                ts = (Order) oin.readObject();
                if (ts.getId().equals(id)) {
                    ts.showOrder();
                }
            }
            oin.close();

        } catch (Exception e) {
            System.out.println("File not found or idk");
        }
    }

    @Override
    public void saveAll(String file) {

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for (Order n : App.orders.getOrders()) {
                oos.writeObject(n);
            }
            oos.flush();
            oos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveById(String file, UUID id) {

        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for (Order n : App.orders.getOrders()) {
                if (n.getId().equals(id))
                oos.writeObject(n);
            }
            oos.flush();
            oos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
