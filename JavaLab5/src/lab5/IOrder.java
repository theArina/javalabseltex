package lab5;

import java.util.UUID;

public interface IOrder {
    void readById(String file, UUID id);
    void saveById(String file, UUID id);
    void readAll(String file);
    void saveAll(String file);
}
