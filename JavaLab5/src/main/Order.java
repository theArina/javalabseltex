package main;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Order implements Serializable {

    private boolean orderStatus;
    private double creationTime;
    private double waitingTime;
    private Credentials customer;
    private ShoppingCart cart;
    UUID id;

    public UUID getId() {
        return id;
    }

    public Order(Credentials customer, ShoppingCart cart) {

        id = UUID.randomUUID();
        this.customer = customer;
        this.cart = cart;
        creationTime = getCreationTimeSec();
        waitingTime = creationTime + Math.random() * 3000;
    }

    public void showOrder() {

        cart.showAllObjects();
        customer.showBuyerData();
    }

    public boolean isOrderProcessed() {

        return orderStatus;// = new Date().getTime() > waitingTime;
    }

    public void setOrderStatus(boolean orderStatus) {

        this.orderStatus = orderStatus;
    }

    public String getCreationTimeStr() {

        return new Date().toString();
    }

    public double getCreationTimeSec() {

        return new Date().getTime();
    }

    public double getCreationTime() {
        return creationTime;
    }

    public double getWaitingTime() {
        return waitingTime;
    }

    public Credentials getCustomer() {
        return customer;
    }

    public ShoppingCart getCart() {
        return cart;
    }
}
