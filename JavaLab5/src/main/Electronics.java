package main;

import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

public abstract class Electronics implements ICrudAction, Serializable {

    private String name;
    private int price;
    private String firm;
    private String model;
    private String os;

    static private int count;
    UUID id;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getFirm() {
        return firm;
    }

    public String getModel() {
        return model;
    }

    public String getOs() {
        return os;
    }

    Electronics() {
        id = UUID.randomUUID();
    }

    Electronics(UUID id) {
        this.id = id;
    }

    @Override
    public void create() {
        this.price = 10000;
        this.name = "SomeName";
        this.firm = "Sony";
        this.model = "12345";
        this.os = "Android";
    }

    @Override
    public void read() {
        System.out.println("Price: " + price);
        System.out.println("Name: " + name);
        System.out.println("Firm: " + firm);
        System.out.println("Model: " + model);
        System.out.println("OS: " + os);
    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter wishful price: ");
        this.price = sc.nextInt();
        sc.nextLine();

        System.out.print("\nEnter name: ");
        this.name = sc.nextLine();

        System.out.print("\nEnter firm: ");
        this.firm = sc.nextLine();

        System.out.print("\nEnter model: ");
        this.model = sc.nextLine();

        System.out.print("\nEnter OS: ");
        this.os = sc.nextLine();
        System.out.println();
    }

    @Override
    public void delete() {
        this.price = 0;
        this.name = "0";
        this.firm = "0";
        this.model = "0";
        this.os = "0";
        count--;
    }
}
