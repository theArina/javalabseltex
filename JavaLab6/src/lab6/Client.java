package lab6;

import main.GenerateOrders;
import main.Order;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;

public class Client extends Thread {

    private DatagramSocket socket;
    private DatagramPacket packet;
    private int UDPport;

    private Socket client;
    private ObjectOutputStream output;

    private byte[] buf = new byte[256];

    public Client(int UDPport) {
        this.UDPport = UDPport;
    }

    public void run() {
        try {
            int TCPport = UDPgetPort();
            int i = 1;
            while (i < 4) {
                System.out.println("-------- " + i + " order ---------");
                TCPconnection(TCPport, new GenerateOrders().makeOrder(i));
                UDPgetPurchaseInfo();
                output.close();
                client.close();
                Thread.sleep(1100);
//                System.out.println("- I'm thread of orders, i've slept for 1100 millis");
                i++;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            closeCrap();
        }
    }

    private int UDPgetPort() throws IOException {
        packet = new DatagramPacket(buf, buf.length); // to receive
        socket = new DatagramSocket(UDPport);
        socket.receive(packet);
        int port = ByteBuffer.wrap(packet.getData()).getInt();
        return port;
    }

    private void UDPgetPurchaseInfo() throws IOException {
        socket.receive(packet);
        double time = ByteBuffer.wrap(packet.getData()).getDouble();
        System.out.println("TIME - " + time);
    }

    private void TCPconnection(int TCPport, Order order) throws IOException {
        connectionToServer(TCPport);
        setupOutputStream();
        sendOrderToServer(order);
    }

    private void connectionToServer(int TCPport) throws IOException {
        System.out.println("Attempting connection...(client)");
        client = new Socket(packet.getAddress(), TCPport);
        System.out.println("Now (client) connected to " + packet.getAddress().getHostName());
    }

    // get stream to send data
    private void setupOutputStream() throws IOException {
        output = new ObjectOutputStream(client.getOutputStream());
        output.flush();
        System.out.println("Output stream is now setup!");
    }

    // send a massage to server
    private void sendOrderToServer(Order order) {
        try {
            output.writeObject(order);
            output.flush();
        } catch (IOException e) {
            System.out.println("ERROR: DUDE I CAN'T SEND THAT");
        }
    }

    private void closeCrap() {
        System.out.println("Closing connections...(client)");
        try {
            output.close();
            client.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

