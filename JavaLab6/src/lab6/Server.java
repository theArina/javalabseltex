package lab6;

import main.Order;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.List;

import static main.App.orders;

public class Server extends Thread {

    private String clientIP;
    private Socket client;

    private DatagramSocket socket;
    private int UDPport;

    private ServerSocket server;
    private ObjectInputStream input;
    private int TCPport;

    public Server(String clientIP, int UDPport, int TCPport) {
        this.clientIP = clientIP;
        this.UDPport = UDPport;
        this.TCPport = TCPport;
    }

    public void run() {
        try {
            UDPsendPort();
            List<Order> ordersList;
            int i = 0;
            while (i < 3) {
                TCPconnection();
                ordersList = orders.getOrders();
                for (Order anOrdersList : ordersList) {
                    if (!orders.checkOrder(anOrdersList)) {
                        anOrdersList.setOrderStatus(true);
                    }
                }
                for (int j = 0; j < ordersList.size(); j++) {
                    if (orders.checkOrder(ordersList.get(j))) {
                        UDPsendPurchaseInfo(ordersList.get(j));
                        ordersList.remove(j);
                    }
                }
                i++;
                input.close();
                server.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeCrap();
        }
    }

    private void UDPsendPort() throws IOException {
        socket = new DatagramSocket();
        InetAddress address = InetAddress.getByName(clientIP);
        byte[] buf = ByteBuffer.allocate(4).putInt(TCPport).array(); // our message
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, UDPport); // for sending
        socket.send(packet);
    }

    private void UDPsendPurchaseInfo(Order order) throws IOException {
        InetAddress address = InetAddress.getByName(clientIP);
        byte[] buf = ByteBuffer.allocate(8).putDouble(order.getWaitingTime()).array(); // our message
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, UDPport); // for sending
        socket.send(packet);
    }

    private void TCPconnection() throws IOException {
        connectionToClient();
        setupInputStream();
        getOrderFromClient();
    }

    private void connectionToClient() throws IOException {
        System.out.println("Waiting for someone to connect...(server)");
        server = new ServerSocket(TCPport);
        client = server.accept();
        System.out.println("Now (server) connected to " + client.getInetAddress().getHostName());
    }

    // get stream to receive data
    private void setupInputStream() throws IOException {
        input = new ObjectInputStream(client.getInputStream());
        System.out.println("Input stream is now setup!");
    }

    // during the chat conversation
    private void getOrderFromClient() throws IOException {
        try {
            Order order = (Order)input.readObject();
            orders.purchaseIssue(order.getCart());
            System.out.println(order);
        } catch (ClassNotFoundException e) {
            System.out.println("Something weird in output!");
        }
    }

    private void closeCrap() {
        System.out.println("Closing input connection...(server)");
        try {
            input.close();
            server.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

