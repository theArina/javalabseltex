package main;

public class GenerateOrders implements Runnable {

    public void run() {
        int i = 1;
        while (true) {
            Test test = new Test();
            ShoppingCart cart = new ShoppingCart();
            test.makeDefaultOrder(cart);
            App.orders.purchaseIssue(cart);
            System.out.println("Created " + i + " order");
            i++;
            try {
                Thread.sleep(1100);
                System.out.println("- I'm thread 1, i've slept for 1100 millis");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Order makeOrder(int orderNumber) {
        ShoppingCart cart = new ShoppingCart();
        new Test().makeDefaultOrder(cart);
        App.orders.purchaseIssue(cart);
        System.out.println("Created " + orderNumber + " order");
        return new Order(new Credentials(), cart);
    }
}
