package main;

import java.util.ArrayList;
import java.util.List;

public class Orders<T extends Order> {

    List<T> orders = new ArrayList<>();

    public List<T> getOrders() {
        return orders;
    }

    public void purchaseIssue(ShoppingCart cart) {

        orders.add((T)new Order(new Credentials(), cart));
    }

    public void cleanOrders() {
        for (int i = 0; i < orders.size(); i++) {
            if (checkOrder(orders.get(i))) {
                orders.remove(i);
            }
        }
    }

    public boolean checkOrder(T n) {
       return n.isOrderProcessed();
    }

    public void showAllOrders() {
        for (T n : orders) {
            n.showOrder();
        }
    }
}
