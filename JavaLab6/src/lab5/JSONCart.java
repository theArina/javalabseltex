package lab5;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import main.Electronics;

public class JSONCart {

    @SerializedName("goods")
    @Expose
    private List<JSONGood> goods = null;
    @SerializedName("ids")
    @Expose
    private List<String> ids = null;

    public List<JSONGood> getGoods() {
        return goods;
    }

    public void setGoods(List<JSONGood> goods) {
        this.goods = goods;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }
}
