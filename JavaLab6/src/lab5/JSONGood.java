package lab5;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JSONGood {

    @SerializedName("phoneCase")
    @Expose
    private Integer phoneCase;
    @SerializedName("PHONECASES")
    @Expose
    private List<String> pHONECASES = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("firm")
    @Expose
    private String firm;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("os")
    @Expose
    private String os;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("simType")
    @Expose
    private Integer simType;
    @SerializedName("simsAmount")
    @Expose
    private Integer simsAmount;
    @SerializedName("SIMTYPES")
    @Expose
    private List<String> sIMTYPES = null;
    @SerializedName("videoProcessor")
    @Expose
    private String videoProcessor;
    @SerializedName("screenResolution")
    @Expose
    private String screenResolution;

    public Integer getPhoneCase() {
        return phoneCase;
    }

    public void setPhoneCase(Integer phoneCase) {
        this.phoneCase = phoneCase;
    }

    public List<String> getPHONECASES() {
        return pHONECASES;
    }

    public void setPHONECASES(List<String> pHONECASES) {
        this.pHONECASES = pHONECASES;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSimType() {
        return simType;
    }

    public void setSimType(Integer simType) {
        this.simType = simType;
    }

    public Integer getSimsAmount() {
        return simsAmount;
    }

    public void setSimsAmount(Integer simsAmount) {
        this.simsAmount = simsAmount;
    }

    public List<String> getSIMTYPES() {
        return sIMTYPES;
    }

    public void setSIMTYPES(List<String> sIMTYPES) {
        this.sIMTYPES = sIMTYPES;
    }

    public String getVideoProcessor() {
        return videoProcessor;
    }

    public void setVideoProcessor(String videoProcessor) {
        this.videoProcessor = videoProcessor;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public void setScreenResolution(String screenResolution) {
        this.screenResolution = screenResolution;
    }

}
