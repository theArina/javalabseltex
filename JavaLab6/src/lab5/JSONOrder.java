package lab5;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import main.Credentials;
import main.ShoppingCart;

import java.util.UUID;

public class JSONOrder {

    @SerializedName("orderStatus")
    @Expose
    private Boolean orderStatus;
    @SerializedName("creationTime")
    @Expose
    private Double creationTime;
    @SerializedName("waitingTime")
    @Expose
    private Double waitingTime;
    @SerializedName("customer")
    @Expose
    private Credentials customer;
    @SerializedName("cart")
    @Expose
    private ShoppingCart cart;
    @SerializedName("id")
    @Expose
    private UUID id;

    public Boolean getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Double getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Double creationTime) {
        this.creationTime = creationTime;
    }

    public Double getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Double waitingTime) {
        this.waitingTime = waitingTime;
    }

    public Credentials getCustomer() {
        return customer;
    }

    public void setCustomer(Credentials customer) {
        this.customer = customer;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

}