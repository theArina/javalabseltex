package lab5;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import main.App;
import main.Order;

import java.io.*;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {

    @Override
    public void readAll(String file) {

        Gson gson = new Gson();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));

            JSONOrders jsonOrders = gson.fromJson(br, JSONOrders.class);

            if (jsonOrders != null) {
                for (Order i: jsonOrders.getOrders()) {
                    System.out.println(i.getId());
                    System.out.println(i.getCustomer().getEmail());
                    System.out.println(i.getCreationTimeStr());
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void readById(String file, UUID id) {

    }

    @Override
    public void saveAll(String file) {

        JSONOrders orders = new JSONOrders();
        orders.setOrders(App.orders.getOrders());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonStr = gson.toJson(orders);

        FileWriter writer = null;

        try {
            writer = new FileWriter(file);
            writer.write(jsonStr);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void saveById(String file, UUID id) {

    }
}
