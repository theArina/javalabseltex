import java.util.Scanner;
import java.util.UUID;

public class Tablets extends Electronics {

    private String videoProcessor;
    private String screenResolution;

    Tablets() {
        super();
    }

    Tablets(UUID id) {
        super(id);
    }

    @Override
    public void create() {
        super.create();
        videoProcessor = "Default";
        screenResolution = "1000x2000";
    }

    @Override
    public void read() {
        System.out.println("\n--Tablet--");
        super.read();
        System.out.println("Video processor: " + videoProcessor);
        System.out.println("Screen resolution: " + screenResolution);
    }

    @Override
    public void update() {
        super.update();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter type of video processor:");
        this.videoProcessor = sc.nextLine();

        System.out.println("Enter wishful screen resolution:");
        this.screenResolution = sc.nextLine();
    }

    @Override
    public void delete() {
        super.delete();
        videoProcessor = "0";
        screenResolution = "0";
    }
}