import java.util.*;

public class Orders<T extends Order> {

    List<T> orders = new ArrayList<>();

    public void purchaseIssue(ShoppingCart cart) {

        orders.add((T)new Order(new Credentials(), cart));
    }

    public void checkOrders() {
        for (T n : orders) {
            if (n.isOrderProcessed()) {
                orders.remove(n);
            }
        }
    }

    public void showAllOrders() {
        for (T n : orders) {
            n.showOrder();
        }
    }
}
