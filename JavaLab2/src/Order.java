import java.util.Date;

public class Order {

//    private boolean orderStatus;
    private double creationTime;
    private double waitingTime;
    private Credentials customer;
    private ShoppingCart cart;

    public Order(Credentials customer, ShoppingCart cart) {

        this.customer = customer;
        this.cart = cart;
        creationTime = getCreationTimeSec();
        waitingTime = creationTime + Math.random() * 1000;
    }

    public void showOrder() {

        cart.showAllObjects();
        customer.showBuyerData();
    }

    public boolean isOrderProcessed() {

        return new Date().getTime() > waitingTime;
    }

    public String getCreationTimeStr() {

        return new Date().toString();
    }

    public double getCreationTimeSec() {

        return new Date().getTime();
    }
}
