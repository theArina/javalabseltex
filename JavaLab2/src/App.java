import java.util.Scanner;

public class App {

    public static Orders<Order> orders = new Orders();


    public static void ifShow(Order order) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to see your order?[0 - no/1 - yes]");
        char check = sc.next().charAt(0);

        if (check == '1') {
            order.showOrder();
        } else if (check == '0') {

        } else {
            System.out.println("What? Try again\n");
            ifShow(order);
        }
    }

    public static void app() {

        ShoppingCart<Electronics> cart = new ShoppingCart();

        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to create objects by default or pick them by yourself?[0/1]");
        char check = sc.next().charAt(0);

        Order order = new Order(new Credentials(), cart);

        if (check == '0') {
            Test test = new Test();
            order = test.makeDefaultOrder(cart);
        } else if (check == '1') {
            cart.pickGood();
            order = new Order(new Credentials(), cart);
        } else {
            System.out.println("Nope... Try again\n");
            app();
        }

        orders.purchaseIssue(cart);
        ifShow(order);

        sc.close();
    }

    public static void main(String args[]) {
        app();
    }
}