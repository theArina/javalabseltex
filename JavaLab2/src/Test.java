public class Test {

    public Order makeDefaultOrder(ShoppingCart cart) {

        int goodsNum = 3;
        Electronics goods[] = new Electronics[goodsNum];
        goods[0] = new Phones();
        goods[1] = new Smartphones();
        goods[2] = new Tablets();

        for (int i = 0; i < goodsNum; i++) {
            goods[i].create();
            cart.addGood(goods[i]);
            System.out.println("Added " + (i + 1) + " object in the cart...");
        }

        return new Order(new Credentials(), cart);
    }
}
