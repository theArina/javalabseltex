import java.util.Scanner;
import java.util.UUID;

public class Credentials {

    private UUID id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;

    Credentials() {
        setDefaultBuyerData();
    }

    public void setBuyerData() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Please set your personal information:");

        System.out.print("\nFirst name: ");
        setFirstName(sc.nextLine());

        System.out.print("\nLast name: ");
        setLastName(sc.nextLine());

        System.out.print("\nPatronymic: ");
        setPatronymic(sc.nextLine());

        System.out.print("\nE-mail");
        setEmail(sc.nextLine());
        System.out.println();
    }

    public void setDefaultBuyerData() {

        id = UUID.randomUUID();

        setFirstName("Somename");

        setLastName("Somelastname");

        setPatronymic("-");

        setEmail("email@mail.com");
    }

    public void showBuyerData() {

        System.out.println("\n--Personal info--");
        System.out.println("First name: " + getFirstName());
        System.out.println("Last name: " + getLastName());
        System.out.println("Patronymic: " + getPatronymic());
        System.out.println("E-mail: " + getEmail());
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
